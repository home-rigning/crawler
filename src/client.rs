use crate::common;
use crate::model;
use crate::store;

use async_trait::async_trait;

#[async_trait]
pub trait Hub: Send + Sync {
    async fn rain(
        &self,
        method: &str,
        days: u64,
    ) -> common::Result<(measurements::Length, measurements::Length)>;
    async fn temp(&self, method: &str, days: u64) -> common::Result<measurements::Temperature>;
    async fn humidity(&self, method: &str, days: u64) -> common::Result<measurements::Humidity>;
    async fn pressure(&self, method: &str, days: u64) -> common::Result<measurements::Pressure>;
    async fn wind(&self, method: &str, days: u64) -> common::Result<measurements::Speed>;
    async fn current(&self) -> common::Result<model::Observation>;
    async fn last(&self, days: u64) -> common::Result<Vec<model::Observation>>;
}

use crate::store::influxdb::InfluxDBQueryResult;

#[async_trait]
impl Hub for store::InfluxDB {
    /// Returns a rain value
    ///
    /// # Arguments
    ///
    /// * `method` - InfluxDB method
    /// * `days` - number of days which define a time range from now - n days
    ///
    /// # Example
    ///
    /// ```
    /// let hub = rigning::store::InfluxDB::new(
    ///                     rigning::model::City::from("Paris"),
    ///                     rigning::store::InfluxDBSettings {
    ///                         url: "http://localhost:8086".to_string(),
    ///                     },
    /// );
    ///
    /// let rain = hub.rain("sum", 30);
    /// ```
    async fn rain(
        &self,
        method: &str,
        days: u64,
    ) -> common::Result<(measurements::Length, measurements::Length)> {
        let query = format!(
            "SELECT {}(\"rain_1h\"), {}(\"rain_3h\") FROM weather WHERE (\"city\" = '{}' AND time > now() - {}d)",
            method, method, self.city, days
        );

        match self
            .query::<(InfluxDBQueryResult<(chrono::DateTime<chrono::Utc>, f64, f64)>,)>(&query)
            .await
        {
            Ok(json) => Ok((
                measurements::Length::from_millimeters(json.results.0.series[0].values[0].1),
                measurements::Length::from_millimeters(json.results.0.series[0].values[0].2),
            )),
            Err(e) => Err(e),
        }
    }

    /// Returns a temperature value
    ///
    /// # Arguments
    ///
    /// * `method` - InfluxDB method
    /// * `days` - number of days which define a time range from now - n days
    ///
    /// # Example
    ///
    /// ```
    /// use rigning::InfluxDBHub;
    ///
    /// let hub = InfluxDBHub::new("http://localhost:8086", "Paris");
    ///
    /// let temp = hub.temp("max", 30);
    /// ```
    async fn temp(&self, method: &str, days: u64) -> common::Result<measurements::Temperature> {
        let query = format!(
            "SELECT {}(\"temp\") FROM weather WHERE (\"city\" = '{}' AND time > now() - {}d)",
            method, self.city, days
        );

        match self
            .query::<(InfluxDBQueryResult<(chrono::DateTime<chrono::Utc>, f64)>,)>(&query)
            .await
        {
            Ok(json) => Ok(measurements::Temperature::from_kelvin(
                json.results.0.series[0].values[0].1,
            )),
            Err(e) => Err(e),
        }
    }

    /// Returns a humidity value
    ///
    /// # Arguments
    ///
    /// * `method` - InfluxDB method
    /// * `days` - number of days which define a time range from now - n days
    ///
    /// # Example
    ///
    /// ```
    /// use rigning::InfluxDBHub;
    ///
    /// let hub = InfluxDBHub::new("http://localhost:8086", "Paris");
    ///
    /// let temp = hub.humidity("max", 30);
    /// ```
    async fn humidity(&self, method: &str, days: u64) -> common::Result<measurements::Humidity> {
        let query = format!(
            "SELECT {}(\"humidity\") FROM weather WHERE (\"city\" = '{}' AND time > now() - {}d)",
            method, self.city, days
        );

        match self
            .query::<(InfluxDBQueryResult<(chrono::DateTime<chrono::Utc>, f64)>,)>(&query)
            .await
        {
            Ok(json) => Ok(measurements::Humidity::from_percent(
                json.results.0.series[0].values[0].1,
            )),
            Err(e) => Err(e),
        }
    }

    /// Returns a pressure value
    ///
    /// # Arguments
    ///
    /// * `method` - InfluxDB method
    /// * `days` - number of days which define a time range from now - n days
    ///
    /// # Example
    ///
    /// ```
    /// use rigning::InfluxDBHub;
    ///
    /// let hub = InfluxDBHub::new("http://localhost:8086", "Paris");
    ///
    /// let pressure = hub.pressure("mean", 30);
    /// ```
    async fn pressure(&self, method: &str, days: u64) -> common::Result<measurements::Pressure> {
        let query = format!(
            "SELECT {}(\"pressure\") FROM weather WHERE (\"city\" = '{}' AND time > now() - {}d)",
            method, self.city, days
        );

        match self
            .query::<(InfluxDBQueryResult<(chrono::DateTime<chrono::Utc>, f64)>,)>(&query)
            .await
        {
            Ok(json) => Ok(measurements::Pressure::from_hectopascals(
                json.results.0.series[0].values[0].1,
            )),
            Err(e) => {
                error!("cannot get pressure: {}", e);
                Err(e)
            }
        }
    }

    /// Returns a wind value
    ///
    /// # Arguments
    ///
    /// * `method` - InfluxDB method
    /// * `days` - number of days which define a time range from now - n days
    ///
    /// # Example
    ///
    /// ```
    /// use rigning::InfluxDBHub;
    ///
    /// let hub = InfluxDBHub::new("http://localhost:8086", "Paris");
    ///
    /// let wind = hub.wind("mean", 30);
    /// ```
    async fn wind(&self, method: &str, days: u64) -> common::Result<measurements::Speed> {
        let query = format!(
            "SELECT {}(\"wind\") FROM weather WHERE (\"city\" = '{}' AND time > now() - {}d)",
            method, self.city, days
        );

        match self
            .query::<(InfluxDBQueryResult<(chrono::DateTime<chrono::Utc>, f64)>,)>(&query)
            .await
        {
            Ok(json) => Ok(measurements::Speed::from_meters_per_second(
                json.results.0.series[0].values[0].1,
            )),
            Err(e) => {
                error!("cannot get wind: {}", e);
                Err(e)
            }
        }
    }

    /// Returns the current Observation
    ///
    /// # Example
    ///
    /// ```
    /// use rigning::InfluxDBHub;
    ///
    /// let hub = InfluxDBHub::new("http://localhost:8086", "Paris");
    ///
    /// let observation = hub.current();
    /// ```
    async fn current(&self) -> common::Result<model::Observation> {
        let query = format!(
            "SELECT last(*) FROM weather WHERE (\"city\" = '{}')",
            self.city
        );

        match self
            .query::<(InfluxDBQueryResult<model::Observation>,)>(&query)
            .await
        {
            Ok(json) => Ok(json.results.0.series[0].values[0].clone()),
            Err(e) => Err(e),
        }
    }
    /// Returns last Observations
    ///
    /// # Arguments
    ///
    /// * `days` - number of days which define a time range from now - n days
    ///
    ///
    /// # Example
    ///
    /// ```
    /// use rigning::InfluxDBHub;
    ///
    /// let hub = InfluxDBHub::new("http://localhost:8086", "Paris");
    ///
    /// let observations = hub.last(30);
    /// ```
    async fn last(&self, days: u64) -> common::Result<Vec<model::Observation>> {
        let query = format!(
            "SELECT \"clouds\",\"humidity\",\"pressure\",\"rain_1h\",\"rain_3h\",\"sunrise\",\"sunset\",\"temp\" FROM weather WHERE (\"city\" = '{}' AND time > now() - {}d)",
            self.city, days
        );

        match self
            .query::<(InfluxDBQueryResult<model::Observation>,)>(&query)
            .await
        {
            Ok(json) => Ok(json.results.0.series[0].values.clone()),
            Err(e) => Err(e),
        }
    }
}

#[async_trait]
impl Hub for store::InfluxDB2 {
    async fn rain(
        &self,
        method: &str,
        days: u64,
    ) -> common::Result<(measurements::Length, measurements::Length)> {
        #[derive(influxdb2::FromDataPoint, Default)]
        struct M {
            rain_1h: f64,
            rain_3h: f64,
        }

        let query = format!(
            "from(bucket: \"rigning\")
               |> range(start: -{}d)
               |> filter(fn: (r) => r._measurement == \"weather\" and r.city == \"{}\")
               |> filter(fn: (r) => r._field == \"rain_1h\" or r._field == \"rain_3h\")
               |> {}()
    ",
            days, self.city, method,
        );
        match self
            .client
            .query::<M>(Some(influxdb2::models::Query::new(query)))
            .await
        {
            Ok(m) => Ok((
                measurements::Length::from_millimeters(m[0].rain_1h),
                measurements::Length::from_millimeters(m[0].rain_3h),
            )),
            Err(e) => Err(Box::new(e)),
        }
    }

    async fn temp(&self, method: &str, days: u64) -> common::Result<measurements::Temperature> {
        #[derive(influxdb2::FromDataPoint, Default)]
        struct M {
            temp: f64,
        }

        let query = format!(
            "from(bucket: \"rigning\")
               |> range(start: -{}d)
               |> filter(fn: (r) => r._measurement == \"weather\" and r.city == \"{}\")
               |> filter(fn: (r) => r._field == \"temp\")
               |> {}()
    ",
            days, self.city, method,
        );
        match self
            .client
            .query::<M>(Some(influxdb2::models::Query::new(query)))
            .await
        {
            Ok(m) => Ok(measurements::Temperature::from_kelvin(m[0].temp)),
            Err(e) => Err(Box::new(e)),
        }
    }
    async fn humidity(&self, method: &str, days: u64) -> common::Result<measurements::Humidity> {
        #[derive(influxdb2::FromDataPoint, Default)]
        struct M {
            humidity: i64,
        }

        let query = format!(
            "from(bucket: \"rigning\")
               |> range(start: -{}d)
               |> filter(fn: (r) => r._measurement == \"weather\" and r.city == \"{}\")
               |> filter(fn: (r) => r._field == \"humidity\")
               |> {}()
    ",
            days, self.city, method,
        );
        match self
            .client
            .query::<M>(Some(influxdb2::models::Query::new(query)))
            .await
        {
            Ok(m) => Ok(measurements::Humidity::from_percent(m[0].humidity as f64)),
            Err(e) => Err(Box::new(e)),
        }
    }
    async fn pressure(&self, method: &str, days: u64) -> common::Result<measurements::Pressure> {
        #[derive(influxdb2::FromDataPoint, Default)]
        struct M {
            pressure: f64,
        }

        let query = format!(
            "from(bucket: \"rigning\")
               |> range(start: -{}d)
               |> filter(fn: (r) => r._measurement == \"weather\" and r.city == \"{}\")
               |> filter(fn: (r) => r._field == \"pressure\")
               |> {}()
    ",
            days, self.city, method,
        );
        match self
            .client
            .query::<M>(Some(influxdb2::models::Query::new(query)))
            .await
        {
            Ok(m) => Ok(measurements::Pressure::from_hectopascals(m[0].pressure)),
            Err(e) => Err(Box::new(e)),
        }
    }
    async fn wind(&self, method: &str, days: u64) -> common::Result<measurements::Speed> {
        #[derive(influxdb2::FromDataPoint, Default)]
        struct M {
            wind: f64,
        }

        let query = format!(
            "from(bucket: \"rigning\")
               |> range(start: -{}d)
               |> filter(fn: (r) => r._measurement == \"weather\" and r.city == \"{}\")
               |> filter(fn: (r) => r._field == \"wind\")
               |> {}()
    ",
            days, self.city, method,
        );
        match self
            .client
            .query::<M>(Some(influxdb2::models::Query::new(query)))
            .await
        {
            Ok(m) => Ok(measurements::Speed::from_meters_per_second(m[0].wind)),
            Err(e) => Err(Box::new(e)),
        }
    }
    async fn current(&self) -> common::Result<model::Observation> {
        Ok(model::Observation {
            time: chrono::Utc::now(),
            clouds: 0,
            humidity: 10,
            pressure: 0,
            rain_1h: 0.0,
            rain_3h: 0.0,
            sunrise: 0,
            sunset: 0,
            temp: 0.0,
            wind: 0.0,
        })
    }
    async fn last(&self, _days: u64) -> common::Result<Vec<model::Observation>> {
        Ok(vec![model::Observation {
            time: chrono::Utc::now(),
            clouds: 0,
            humidity: 10,
            pressure: 0,
            rain_1h: 0.0,
            rain_3h: 0.0,
            sunrise: 0,
            sunset: 0,
            temp: 0.0,
            wind: 0.0,
        }])
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::str::FromStr;

    #[test]
    fn test_json() {
        let res = serde_json::from_str::<
            store::influxdb::InfluxDBQueryResults<(
                store::influxdb::InfluxDBQueryResult<(chrono::DateTime<chrono::Utc>, f64)>,
            )>,
        >(
            "{
                \"results\":[
                {
                    \"statement_id\":0,
                    \"series\":[
                        {
                            \"name\":\"weather\",
                            \"columns\":[\"time\",\"sum\"],
                            \"values\":[[\"1970-01-01T00:00:00Z\",4.669999912381172]]
                        }]
                }]
            }",
        )
        .unwrap();

        assert_eq!(res.results.0.series[0].values[0].1, 4.669999912381172);
    }

    #[test_log::test(tokio::test)]
    async fn test_influxdb_rain() {
        let hub = crate::store::influxdb::InfluxDB::new(
            crate::model::City::from_str("Oullins,FR").unwrap(),
            crate::store::influxdb::InfluxDBSettings {
                url: "http://influxdb.rol4life.here".to_string(),
            },
        )
        .await;

        assert!(hub.rain("sum", 30).await.is_ok());
    }

    #[test_log::test(tokio::test)]
    async fn test_influxdb_temp_max() {
        let hub = store::influxdb::InfluxDB::new(
            model::City::from_str("Oullins,FR").unwrap(),
            store::influxdb::InfluxDBSettings {
                url: "http://influxdb.rol4life.here".to_string(),
            },
        )
        .await;

        assert!(hub.temp("max", 30).await.is_ok());
    }

    #[test_log::test(tokio::test)]
    async fn test_influxdb_temp_min() {
        let hub = store::influxdb::InfluxDB::new(
            model::City::from_str("Oullins,FR").unwrap(),
            store::influxdb::InfluxDBSettings {
                url: "http://influxdb.rol4life.here".to_string(),
            },
        )
        .await;

        assert!(hub.temp("min", 30).await.is_ok());
    }

    #[test_log::test(tokio::test)]
    async fn test_influxdb_pressure() {
        let hub = store::influxdb::InfluxDB::new(
            model::City::from_str("Oullins,FR").unwrap(),
            store::influxdb::InfluxDBSettings {
                url: "http://influxdb.rol4life.here".to_string(),
            },
        )
        .await;

        assert!(hub.pressure("median", 30).await.is_ok());
    }

    #[test_log::test(tokio::test)]
    async fn test_influxdb_current() {
        let hub = store::influxdb::InfluxDB::new(
            model::City::from_str("Oullins,FR").unwrap(),
            store::influxdb::InfluxDBSettings {
                url: "http://influxdb.rol4life.here".to_string(),
            },
        )
        .await;

        assert!(hub.current().await.is_ok());
    }

    #[test_log::test(tokio::test)]
    async fn test_influxdb_last() {
        let hub = store::influxdb::InfluxDB::new(
            model::City::from_str("Oullins,FR").unwrap(),
            store::influxdb::InfluxDBSettings {
                url: "http://influxdb.rol4life.here".to_string(),
            },
        )
        .await;

        assert!(hub.last(30).await.is_ok());
    }

    #[test_log::test(tokio::test)]
    async fn test_influxdb2_rain() {
        let hub = crate::store::influxdb2::InfluxDB2::new(
            crate::model::City::from_str("Oullins,FR").unwrap(),
            crate::store::influxdb2::InfluxDB2Settings {
                url: "https://eu-central-1-1.aws.cloud2.influxdata.com".to_string(),
                org: "6c4cf65a3a001390".to_string(),
                token: "gP7ZCQV7a0Vh49IH1xmAWnH6sPUPnQ3pD3w7LIXWO595AgLykB_3anwvZeXVIkYsPFDgxX_ha78oB_cmV8gqTA==".to_string(),
            },
        )
        .await;

        assert!(hub.rain("sum", 30).await.is_ok());
    }
}
