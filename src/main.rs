#[macro_use]
extern crate log;

use clap::Parser;
use clokwerk::TimeUnits;

mod common;
mod model;
mod storage;
mod store;

async fn api(req: hyper::Request<hyper::Body>) -> common::Result<hyper::Response<hyper::Body>> {
    match (req.method(), req.uri().path()) {
        // FIXME: add missing routes
        _ => {
            // Return 404 not found response.
            Ok(hyper::Response::builder()
                .status(hyper::StatusCode::NOT_FOUND)
                .body("Not Found".into())
                .unwrap())
        }
    }
}

fn get_owm_api_key(args: Arguments) -> Result<String, &'static str> {
    match (std::env::var("OWM_API_KEY"), args.owm_api_key) {
        (Ok(key), _) => Ok(key),
        (Err(_), Some(key)) => Ok(key),
        (Err(_), None) => Err("Missing key"),
    }
}

fn get_storage_settings(args: Arguments) -> Option<store::Settings> {
    match args.command {
        Commands::Graphite { url } => {
            Some(store::Settings::Graphite(store::GraphiteSettings { url }))
        }
        Commands::Influxdb { url } => {
            Some(store::Settings::InfluxDB(store::InfluxDBSettings { url }))
        }
        Commands::Influxdb2 { url, org, token } => {
            Some(store::Settings::InfluxDB2(store::InfluxDB2Settings {
                url,
                org,
                token,
            }))
        }
        Commands::Dummy {} => None,
    }
}

#[derive(clap::Subcommand, Debug, Clone)]
enum Commands {
    /// Graphite options
    Graphite {
        #[arg(long)]
        /// Graphite URL
        url: String,
    },
    /// influxDB options
    Influxdb {
        #[arg(long)]
        /// InfluxDB URL
        url: String,
    },
    /// influxDB2 options
    Influxdb2 {
        #[arg(long)]
        /// InfluxDB2 url
        url: String,
        #[arg(long)]
        /// InfluxDB2 organisation
        org: String,
        #[arg(long)]
        /// InfluxDB2 token
        token: String,
    },
    /// Dummy options
    Dummy {},
}

#[derive(clap::Parser, Debug, Clone)]
#[clap(author, version, about)]
/// Relay API
struct Arguments {
    #[arg(default_value_t = std::net::IpAddr::V4(std::net::Ipv4Addr::new(0, 0, 0, 0)), short, long)]
    /// listen HTTP port
    listen_ip: std::net::IpAddr,

    #[clap(default_value_t = 80, short, long)]
    /// listen HTTP port
    port: u16,

    #[command(subcommand)]
    command: Commands,

    #[arg(long)]
    /// OpenWeatherMap API key
    owm_api_key: Option<String>,

    #[arg(default_value_t = 3600, long)]
    /// Crawl interval
    every: u32,

    /// city code
    city: model::City,
}

#[tokio::main]
pub async fn main() -> common::Result<(), hyper::Error> {
    env_logger::init();
    let args = Arguments::parse();

    let addr = std::net::SocketAddr::new(args.listen_ip, args.port);
    let city = args.city.clone();
    let api_key = match get_owm_api_key(args.clone()) {
        Ok(key) => key,
        Err(msg) => panic!("cannot get OWM API key: {msg}", msg = msg),
    };

    info!("Rigning is starting for {}...", city);

    let (store_tx, store_rx) = tokio::sync::mpsc::channel(100);
    let mut scheduler = clokwerk::Scheduler::new();

    scheduler.every(args.every.seconds()).run(move || {
        //scheduler.every(1.minute()).run(move || {
        let hub_store_tx = store_tx.clone();
        let hub = owm::hub::WeatherHub::new(hyper::Client::new(), &api_key);
        let city = city.clone();

        tokio::spawn(async move {
            let obs = hub
                .current()
                .by_name(&city.name, city.country.as_ref().map(|s| s.as_str()))
                .await;

            if let Err(e) = hub_store_tx.send(obs).await {
                error!("receiver drops observation for {}: {}", city.name, e);
            }
        });
    });

    tokio::spawn(async move {
        loop {
            scheduler.run_pending();

            tokio::time::sleep(std::time::Duration::from_millis(1000)).await;
        }
    });

    let city = args.city.clone();
    let settings = get_storage_settings(args);
    let store = storage::factory(city, settings).await.unwrap();
    tokio::spawn(storage::store(store, store_rx));

    let service = hyper::service::make_service_fn(|_| async {
        Ok::<_, common::GenericError>(hyper::service::service_fn(move |req| {
            info!("{} {}", req.method(), req.uri().path());

            api(req)
        }))
    });

    let server = hyper::Server::bind(&addr).serve(service);

    info!("Rigning is listening on http://{}...", addr);

    server.await
}
