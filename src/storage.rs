use crate::common;
use crate::model;
use crate::store;

use async_trait::async_trait;

#[async_trait]
pub trait Store: Send + Sync {
    async fn commit(&self, obs: model::Observation);
}

#[async_trait]
impl Store for store::Dummy {
    async fn commit(&self, obs: model::Observation) {
        info!("dummy Store: commit {}", obs);
    }
}

use dipstick::InputScope;

#[async_trait]
impl Store for store::Graphite {
    async fn commit(&self, obs: model::Observation) {
        self.bucket.gauge("clouds").value(obs.clouds);
        self.bucket.gauge("humidity").value(obs.humidity);
        self.bucket.gauge("pressure").value(obs.pressure);
        self.bucket.gauge("rain_1h").value(obs.rain_1h);
        self.bucket.gauge("rain_3h").value(obs.rain_3h);
        self.bucket.gauge("sunrise").value(obs.sunrise);
        self.bucket.gauge("sunset").value(obs.sunset);
        self.bucket.gauge("temp").value(obs.temp);
        self.bucket.gauge("wind").value(obs.wind);

        info!("graphite Store: commit {}", obs);
    }
}

use influxdb::InfluxDbWriteable;

#[async_trait]
impl Store for store::InfluxDB {
    async fn commit(&self, obs: model::Observation) {
        let query = influxdb::Timestamp::from(obs.time)
            .into_query("weather")
            .add_tag("city", self.city.to_string())
            .add_field("clouds", obs.clouds)
            .add_field("humidity", obs.humidity)
            .add_field("pressure", obs.pressure)
            .add_field("rain_1h", obs.rain_1h)
            .add_field("rain_3h", obs.rain_3h)
            .add_field("sunrise", obs.sunrise)
            .add_field("sunset", obs.sunset)
            .add_field("temp", obs.temp)
            .add_field("wind", obs.wind);

        match self.client.query(query).await {
            Ok(_) => info!("influxDB Store: commit {}", obs),
            Err(err) => error!("influxDB Store: failed to commit {}: {}", obs, err),
        }
    }
}

#[async_trait]
impl Store for store::InfluxDB2 {
    async fn commit(&self, obs: model::Observation) {
        let points = vec![influxdb2::models::DataPoint::builder("weather")
            .timestamp(obs.time.timestamp_nanos_opt().unwrap())
            .tag("city", self.city.to_string())
            .field("clouds", obs.clouds)
            .field("humidity", obs.humidity)
            .field("pressure", obs.pressure)
            .field("rain_1h", obs.rain_1h)
            .field("rain_3h", obs.rain_3h)
            .field("sunrise", obs.sunrise)
            .field("sunset", obs.sunset)
            .field("temp", obs.temp)
            .field("wind", obs.wind)
            .build()
            .unwrap()];

        match self
            .client
            .write("rigning", futures::stream::iter(points))
            .await
        {
            Ok(_) => info!("influxDB Store: commit {}", obs),
            Err(err) => error!("influxDB Store: failed to commit {}: {}", obs, err),
        }
    }
}

pub async fn store(
    store: Box<dyn Store>,
    mut rx: tokio::sync::mpsc::Receiver<common::Result<owm::data::WeatherInfo>>,
) {
    while let Some(data) = rx.recv().await {
        match data {
            Ok(data) => {
                let obs = model::Observation::from(data);

                store.commit(obs.clone()).await;
            }
            Err(e) => error!("got an error {}", e),
        }
    }
}

pub async fn factory(
    city: model::City,
    settings: Option<store::Settings>,
) -> Option<Box<dyn Store>> {
    match settings {
        Some(store::Settings::Graphite(settings)) => {
            Some(Box::new(store::Graphite::new(city, settings).await))
        }
        Some(store::Settings::InfluxDB(settings)) => {
            Some(Box::new(store::InfluxDB::new(city, settings).await))
        }
        Some(store::Settings::InfluxDB2(settings)) => {
            Some(Box::new(store::InfluxDB2::new(city, settings).await))
        }
        None => Some(Box::new(store::Dummy::new(city).await)),
    }
}
