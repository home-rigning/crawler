pub mod dummy;
pub use dummy::Dummy;

pub mod graphite;
pub use graphite::{Graphite, GraphiteSettings};

pub mod influxdb;
pub use influxdb::{InfluxDB, InfluxDBSettings};

pub mod influxdb2;
pub use influxdb2::{InfluxDB2, InfluxDB2Settings};

pub enum Settings {
    Graphite(GraphiteSettings),
    InfluxDB(InfluxDBSettings),
    InfluxDB2(InfluxDB2Settings),
}
