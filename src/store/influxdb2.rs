use crate::model;

pub struct InfluxDB2 {
    pub client: influxdb2::Client,
    pub city: model::City,
}

pub struct InfluxDB2Settings {
    pub url: String,
    pub org: String,
    pub token: String,
}

impl InfluxDB2 {
    pub async fn new(city: model::City, settings: InfluxDB2Settings) -> Self {
        info!("Create a influxDB2 Store for {city}");
        let client = influxdb2::Client::new(settings.url, settings.org, settings.token);

        match client.ready().await {
            Ok(_) => info!("Connected to influxDB2"),
            Err(err) => panic!("Cannot get health from influxDB2: {}", err),
        }

        InfluxDB2 { client, city }
    }
}
