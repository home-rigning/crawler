use crate::common;
use crate::model;

pub struct InfluxDB {
    pub client: influxdb::Client,
    pub city: model::City,
}

pub struct InfluxDBSettings {
    pub url: String,
}

#[derive(Debug)]
pub struct InfluxDBError {
    error: influxdb::Error,
}

impl std::fmt::Display for InfluxDBError {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "influxDB Error: {}", self.error)
    }
}

impl std::error::Error for InfluxDBError {}

#[derive(Debug, Clone, PartialEq, serde::Deserialize)]
pub struct InfluxDBSerie<T> {
    pub name: String,
    pub columns: Vec<String>,
    pub values: Vec<T>,
}

#[derive(Clone, Debug, PartialEq, serde::Deserialize)]
pub struct InfluxDBQueryResult<T> {
    pub statement_id: u64,
    pub series: Vec<InfluxDBSerie<T>>,
}

#[derive(Clone, Debug, PartialEq, serde::Deserialize)]
pub struct InfluxDBQueryResults<T> {
    pub results: T,
}

impl InfluxDB {
    pub async fn new(city: model::City, settings: InfluxDBSettings) -> Self {
        info!("Create a influxDB Store for {city}");
        let client = influxdb::Client::new(settings.url, "rigning");

        match client.ping().await {
            Ok((build_type, version)) => {
                info!("Connected to influxDB (build {build_type}, version {version})")
            }
            Err(err) => panic!("Cannot ping influxDB: {}", err),
        }

        InfluxDB { client, city }
    }

    pub async fn query<T: serde::de::DeserializeOwned>(
        &self,
        query: &str,
    ) -> common::Result<InfluxDBQueryResults<T>> {
        let read_query = influxdb::ReadQuery::new(query);

        debug!("query: '{}'", query);
        match self.client.query(&read_query).await {
            Ok(value) => {
                debug!("reply: {}", value);

                match serde_json::from_str::<InfluxDBQueryResults<T>>(&value) {
                    Ok(json) => Ok(json),
                    Err(e) => Err(Box::new(e)),
                }
            }
            Err(e) => {
                error!("query failed: {}", e);
                Err(Box::new(InfluxDBError { error: e }))
            }
        }
    }
}
