use crate::model;

pub struct Dummy;

impl Dummy {
    pub async fn new(city: model::City) -> Self {
        info!("Create a dummy Store for {city}");

        Dummy {}
    }
}
