use crate::model;

use dipstick::{Prefixed, ScheduleFlush};

pub struct Graphite {
    pub bucket: dipstick::AtomicBucket,
}

pub struct GraphiteSettings {
    pub url: String,
}

impl Graphite {
    pub async fn new(city: model::City, settings: GraphiteSettings) -> Self {
        info!("Create a graphite Store for {city}");

        let bucket = dipstick::AtomicBucket::new().named(city);
        bucket.drain(
            dipstick::Graphite::send_to(settings.url)
                .expect("Socket")
                .named("rigning"),
        );
        bucket.flush_every(std::time::Duration::from_secs(3));

        return Graphite { bucket };
    }
}
