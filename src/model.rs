use std::str::FromStr;

#[derive(Clone, Debug, serde::Serialize, serde::Deserialize)]
pub struct Observation {
    pub time: chrono::DateTime<chrono::Utc>,

    pub clouds: i64,
    pub humidity: i64,
    pub pressure: i64,
    pub rain_1h: f64,
    pub rain_3h: f64,
    pub sunrise: i64,
    pub sunset: i64,
    pub temp: f64,
    pub wind: f64,
}

impl Observation {
    pub fn humidity(&self) -> measurements::Humidity {
        measurements::Humidity::from_percent(self.humidity as f64)
    }

    pub fn pressure(&self) -> measurements::Pressure {
        measurements::Pressure::from_hectopascals(self.pressure as f64)
    }

    pub fn rain(&self) -> (measurements::Length, measurements::Length) {
        (
            measurements::Length::from_millimetres(self.rain_1h),
            measurements::Length::from_millimetres(self.rain_3h),
        )
    }

    pub fn temp(&self) -> measurements::Temperature {
        measurements::Temperature::from_kelvin(self.temp)
    }

    pub fn wind(&self) -> measurements::Speed {
        measurements::Speed::from_meters_per_second(self.wind)
    }
}

impl std::convert::From<owm::data::WeatherInfo> for Observation {
    fn from(data: owm::data::WeatherInfo) -> Self {
        Self {
            time: chrono::Utc::now(),
            sunrise: match &data.sys {
                Some(sys) => sys.sunrise.unwrap_or(0).into(),
                None => 0,
            },
            sunset: match &data.sys {
                Some(sys) => sys.sunset.unwrap_or(0),
                None => 0,
            },
            temp: match &data.main {
                Some(main) => main.temp.unwrap_or(0.0).into(),
                None => 0.0,
            },
            humidity: match &data.main {
                Some(main) => main.humidity.unwrap_or(0).into(),
                None => 0,
            },
            pressure: match &data.main {
                Some(main) => main.pressure.unwrap_or(0).into(),
                None => 0,
            },
            rain_1h: match &data.rain {
                Some(rain) => rain.one_hour.unwrap_or(0.0).into(),
                None => 0.0,
            },
            rain_3h: match &data.rain {
                Some(rain) => rain.three_hours.unwrap_or(0.0).into(),
                None => 0.0,
            },
            clouds: match &data.clouds {
                Some(clouds) => clouds.all.unwrap_or(0).into(),
                None => 0,
            },
            wind: match &data.wind {
                Some(wind) => wind.speed.unwrap_or(0.0).into(),
                None => 0.0,
            },
        }
    }
}

impl std::fmt::Display for Observation {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(
            f,
            "datetime: {} {}",
            self.time,
            serde_json::to_string(self).unwrap()
        )
    }
}

#[derive(serde::Serialize, serde::Deserialize, Clone, Debug)]
pub struct City {
    pub name: String,
    pub country: Option<String>,
}

impl City {
    pub fn new(city: &str) -> Self {
        City::from_str(city).unwrap()
    }
}

impl std::fmt::Display for City {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        match &self.country {
            Some(country) => write!(f, "{},{}", self.name, country),
            None => write!(f, "{}", self.name),
        }
    }
}

impl std::str::FromStr for City {
    type Err = std::convert::Infallible;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let mut split = s.split(",").map(|x| x.to_string()).collect::<Vec<String>>();

        Ok(City {
            name: split[0].to_string(),
            country: {
                split.remove(0);
                split.pop()
            },
        })
    }
}

impl From<City> for std::string::String {
    fn from(city: City) -> Self {
        city.name + "," + &city.country.unwrap_or("unknown".to_string())
    }
}

#[cfg(test)]
mod tests {
    mod observation {
        use crate::model::Observation;

        #[test]
        fn measurements_humidity() {
            let obs = Observation {
                time: chrono::Utc::now(),
                clouds: 0,
                humidity: 10,
                pressure: 0,
                rain_1h: 0.0,
                rain_3h: 0.0,
                sunrise: 0,
                sunset: 0,
                temp: 0.0,
                wind: 0.0,
            };
            assert_eq!(obs.humidity(), measurements::Humidity::from_ratio(0.1));
        }

        #[test]
        fn measurements_pressure() {
            let obs = Observation {
                time: chrono::Utc::now(),
                clouds: 0,
                humidity: 0,
                pressure: 10,
                rain_1h: 0.0,
                rain_3h: 0.0,
                sunrise: 0,
                sunset: 0,
                temp: 0.0,
                wind: 0.0,
            };
            assert_eq!(
                obs.pressure(),
                measurements::Pressure::from_hectopascals(10.0)
            );
        }

        #[test]
        fn measurements_rain() {
            let obs = Observation {
                time: chrono::Utc::now(),
                clouds: 0,
                humidity: 0,
                pressure: 0,
                rain_1h: 1.0,
                rain_3h: 3.0,
                sunrise: 0,
                sunset: 0,
                temp: 0.0,
                wind: 0.0,
            };
            assert_eq!(
                obs.rain(),
                (
                    measurements::Length::from_millimetres(1.0),
                    measurements::Length::from_millimetres(3.0),
                )
            );
        }

        #[test]
        fn measurements_temp() {
            let obs = Observation {
                time: chrono::Utc::now(),
                clouds: 0,
                humidity: 0,
                pressure: 0,
                rain_1h: 0.0,
                rain_3h: 0.0,
                sunrise: 0,
                sunset: 0,
                temp: 10.0,
                wind: 0.0,
            };
            assert_eq!(obs.temp(), measurements::Temperature::from_kelvin(10.0));
        }

        #[test]
        fn measurements_wind() {
            let obs = Observation {
                time: chrono::Utc::now(),
                clouds: 0,
                humidity: 0,
                pressure: 0,
                rain_1h: 0.0,
                rain_3h: 0.0,
                sunrise: 0,
                sunset: 0,
                temp: 0.0,
                wind: 10.0,
            };
            assert_eq!(
                obs.wind(),
                measurements::Speed::from_meters_per_second(10.0)
            );
        }
    }

    mod city {
        use crate::model::City;

        #[test]
        fn full() {
            let city = City::new("Paris,Fr");
            assert_eq!(city.name, "Paris");
            assert_eq!(city.country, Some("Fr".to_string()));
        }

        #[test]
        fn partial() {
            let city = City::new("Paris");
            assert_eq!(city.name, "Paris".to_string());
            assert_eq!(city.country, None);
        }
    }
}
