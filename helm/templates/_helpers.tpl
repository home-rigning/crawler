{{/*
Expand the name of the chart.
*/}}
{{- define "crawler.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "crawler.fullname" -}}
{{- if .Values.fullnameOverride }}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- $name := default .Chart.Name .Values.nameOverride }}
{{- if contains $name .Release.Name }}
{{- .Release.Name | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" }}
{{- end }}
{{- end }}
{{- end }}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "crawler.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Common labels
*/}}
{{- define "crawler.labels" -}}
helm.sh/chart: {{ include "crawler.chart" . }}
{{ include "crawler.selectorLabels" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end }}

{{/*
Selector labels
*/}}
{{- define "crawler.selectorLabels" -}}
app.kubernetes.io/name: {{ include "crawler.name" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end }}

{{/*
Create the name of the service account to use
*/}}
{{- define "crawler.serviceAccountName" -}}
{{- if .Values.serviceAccount.create }}
{{- default (include "crawler.fullname" .) .Values.serviceAccount.name }}
{{- else }}
{{- default "default" .Values.serviceAccount.name }}
{{- end }}
{{- end }}


{{/*
Create args of the crawler container
*/}}
{{- define "crawler.args" -}}
  {{- $args := list -}}
  {{- if .Values.listenIp }}
    {{- $args = concat $args ( list "--listen-ip" (.Values.listenIp | toString) ) -}}
  {{- end }}
  {{- if .Values.port }}
    {{- $args = concat $args ( list "--port" (.Values.port | toString) ) -}}
  {{- end }}
  {{- $args = concat $args ( list (default "Paris,FR" .Values.city | toString) ) -}}
  {{- with .Values.backend }}
    {{- $args = concat $args ( list (default "dummy" .type | toString) ) -}}
    {{- range $key, $value := .options }}
      {{- $args = concat $args ( list (printf "--%s" $key | toString) ($value | toString) ) -}}
    {{- end }}
  {{- end }}
  {{- $args | toJson }}
{{- end }}
