FROM --platform=$TARGETPLATFORM alpine

ARG TARGETPLATFORM

COPY target/${TARGETPLATFORM}/rigning /rigning

ENTRYPOINT ["/rigning"]
